/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author nanda
 */
public class Product {
    private String ProductName;
    private int ModelNumber;
    private int Price;
    private  int count=0;
    
    public Product() {
    count++;
    ModelNumber = count;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public int getModelNumber() {
        return ModelNumber;
    }

    public void setModelNumber(int ModelNumber) {
        this.ModelNumber = ModelNumber;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    
}
