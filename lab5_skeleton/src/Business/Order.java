/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;

/**
 *
 * @author nanda
 */
public class Order {
    
    private ArrayList<OrderItem> OrderItemList;
    private int orderNUm;
    private static int   count = 0;

    public ArrayList<OrderItem> getOrderItemList() {
        return OrderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> OrderItemList) {
        this.OrderItemList = OrderItemList;
    }

    public int getOrderNUm() {
        return orderNUm;
    }

    public void setOrderNUm(int orderNUm) {
        this.orderNUm = orderNUm;
    }
    
    public Order(){
        count++;
        orderNUm = count;
      OrderItemList = new ArrayList<OrderItem>();  
      
}
    public OrderItem addOrderItem(Product p,int q,int r){
        
        OrderItem o = new OrderItem();
        o.setProduct(p);
        o.setQuantity(q);
        o.setSalesprice(r);
        OrderItemList.add(o);
        return o;
    }
    
    public void removeProduct(OrderItem o){
        OrderItemList.remove(o);
    }
    
   
}
