/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;
import java.util.ArrayList;

/**
 *
 * @author nanda
 */
public class ProductDirectory {
    private ArrayList<Product> productListComCat;
     private ArrayList<Product> productListprint;
      private ArrayList<Product> productListsoft;
       private ArrayList<Product> productListhard;
       
        public ProductDirectory() {
         this.productListComCat= new ArrayList<Product>();
          this.productListprint= new ArrayList<Product>();
           this.productListsoft= new ArrayList<Product>();
            this.productListhard= new ArrayList<Product>();
          
         
    }

    public ArrayList<Product> getProductListprint() {
        return productListprint;
    }

    public void setProductListprint(ArrayList<Product> productListprint) {
        this.productListprint = productListprint;
    }

    public ArrayList<Product> getProductListsoft() {
        return productListsoft;
    }

    public void setProductListsoft(ArrayList<Product> productListsoft) {
        this.productListsoft = productListsoft;
    }

    public ArrayList<Product> getProductListhard() {
        return productListhard;
    }

    public void setProductListhard(ArrayList<Product> productListhard) {
        this.productListhard = productListhard;
    }

   

    public ArrayList<Product> getProductListComCat() {
        return productListComCat;
    }

    public void setProductListComCat(ArrayList<Product> productListComCat) {
        this.productListComCat = productListComCat;
    }

    
     
public Product addComProduct(){
Product product= new Product();
productListComCat.add(product);
return product;
}
public Product addHardProduct(){
Product product= new Product();
productListhard.add(product);
return product;
}
public Product addSoftProduct(){
Product product= new Product();
productListsoft.add(product);
return product;
}
public Product addPrintProduct(){
Product product= new Product();
productListprint.add(product);
return product;
}

public void deleteProductComp(Product product){
    productListComCat.remove(product);
}
public void deleteProductPrint(Product product){
    productListprint.remove(product);
}
public void deleteProductHard(Product product){
    productListhard.remove(product);
}
public void deleteProductSoft(Product product){
    productListsoft.remove(product);
}
public Product searchProductComp(String ModelNumber){
    for(Product product: productListComCat){
        if(product.getModelNumber().equals(ModelNumber)){
            return product;
        }
    }
    return null;
}
public Product searchProductSoft(String ModelNumber){
    for(Product product: productListsoft){
        if(product.getModelNumber().equals(ModelNumber)){
            return product;
        }
    }
    return null;
}
public Product searchProductHard(String ModelNumber){
    for(Product product: productListhard){
        if(product.getModelNumber().equals(ModelNumber)){
            return product;
        }
    }
    return null;
}
public Product searchProductPrint(String ModelNumber){
    for(Product product: productListprint){
        if(product.getModelNumber().equals(ModelNumber)){
            return product;
        }
    }
    return null;
}
public Product searchProductCompname(String ProductName){
    for(Product product: productListComCat){
        if(product.getModelNumber().equals(ProductName)){
            return product;
        }
    }
    return null;
}
public Product searchProductvnamecomp(String VendorName){
    for(Product product: productListComCat){
        if(product.getModelNumber().equals(VendorName)){
            return product;
        }
    }
    return null;
}



}

