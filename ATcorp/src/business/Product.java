/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *a)	
d)	A list of product features and  (textual statements) 


 * @author nanda
 */
public class Product {
    
    private String ProductName;
    private String ModelNumber;
    private String ProductDescription;
    private int ProductBasePrice;
    private int ProductCeilingPrice;
    private int ProductFloorPrice;
    private String VendorName;
    private String ProductFeaturesBenfits;

    public String getProductFeaturesBenfits() {
        return ProductFeaturesBenfits;
    }

    public void setProductFeaturesBenfits(String ProductFeaturesBenfits) {
        this.ProductFeaturesBenfits = ProductFeaturesBenfits;
    }
    

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public String getModelNumber() {
        return ModelNumber;
    }

    public void setModelNumber(String ModelNumber) {
        this.ModelNumber = ModelNumber;
    }

    public String getProductDescription() {
        return ProductDescription;
    }

    public void setProductDescription(String ProductDescription) {
        this.ProductDescription = ProductDescription;
    }

    public int getProductBasePrice() {
        return ProductBasePrice;
    }

    public void setProductBasePrice(int ProductBasePrice) {
        this.ProductBasePrice = ProductBasePrice;
    }

    public int getProductCeilingPrice() {
        return ProductCeilingPrice;
    }

    public void setProductCeilingPrice(int ProductCeilingPrice) {
        this.ProductCeilingPrice = ProductCeilingPrice;
    }

    public int getProductFloorPrice() {
        return ProductFloorPrice;
    }

    public void setProductFloorPrice(int ProductFloorPrice) {
        this.ProductFloorPrice = ProductFloorPrice;
    }

    public String getVendorName() {
        return VendorName;
    }

    public void setVendorName(String VendorName) {
        this.VendorName = VendorName;
    }
@Override
    public String toString(){
        return this.ProductName;
    }
    
    
    
}
