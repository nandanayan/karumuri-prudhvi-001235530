/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nanda
 */
public class univList {
    List<University> univList;
    

    public univList() {
        univList = new ArrayList<>();
      
    }

    public List<University> getUnivList() {
        return univList;
    }

    public void setUnivList(List<University> univList) {
        this.univList = univList;
    }

    
   
     public University addUniv() {
        University university = new University();
        univList.add(university);
        return university;
    
    }
}
