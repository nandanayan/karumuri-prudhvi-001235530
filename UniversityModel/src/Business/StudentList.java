/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nanda
 */
public class StudentList {
    List<Student> studentList;
     private GpaStudents gpaStudents;

    public StudentList() {
        studentList = new ArrayList<>();
        gpaStudents = new GpaStudents();
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }
public Student addStudent() {
        Student student = new Student();
        studentList.add(student);
        return student;
    }

    public GpaStudents getGpaStudents() {
        return gpaStudents;
    }

    public void setGpaStudents(GpaStudents gpaStudents) {
        this.gpaStudents = gpaStudents;
    }

     
    
}
