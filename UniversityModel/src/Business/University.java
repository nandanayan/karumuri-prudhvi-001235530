/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author nanda
 */
public class University {
    
 
     private String UnivName;
    private String Region;
    private String accredation;
    private String Ranking;

    public String getUnivName() {
        return UnivName;
    }

    public void setUnivName(String UnivName) {
        this.UnivName = UnivName;
    }

    public String getRegion() {
        return Region;
    }

    public void setRegion(String Region) {
        this.Region = Region;
    }

    public String getAccredation() {
        return accredation;
    }

    public void setAccredation(String accredation) {
        this.accredation = accredation;
    }

    public String getRanking() {
        return Ranking;
    }

    public void setRanking(String Ranking) {
        this.Ranking = Ranking;
    }
    
    
    
}
