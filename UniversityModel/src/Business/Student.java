/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author nanda
 */
public class Student {
     private String firstName;
    private String lastName;
    private int id;
 
    private GpaStudents gpaStudents;
    public Student() {
        gpaStudents = new GpaStudents();
    }

    public GpaStudents getGpaStudents() {
        return gpaStudents;
    }

    public void setGpaStudents(GpaStudents gpaStudents) {
        this.gpaStudents = gpaStudents;
    }
    

    
    

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

   
    
}
