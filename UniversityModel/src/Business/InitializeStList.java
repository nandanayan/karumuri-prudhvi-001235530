/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;


/**
 *
 * @author nanda
 */
public class InitializeStList {
    
    public static UnivListreg1 initializeUnivListreg1() { 
         UnivListreg1 Univlistreg1 = new UnivListreg1(); 
         University university = Univlistreg1.addUniv();
         university.setAccredation("Council for Higher Education Accreditation (CHEA)");
          university.setUnivName("University of Virginia ");
          university.setRegion("South");
          university.setRanking("5");
          university = Univlistreg1.addUniv();
          university.setAccredation("INational Association of State Universities and Land Grant Colleges");
          university.setUnivName("Tufts University");
          university.setRegion("South");
          university.setRanking("6");
          university = Univlistreg1.addUniv();
          university.setAccredation("National Association of State Universities and Land Grant Colleges");
          university.setUnivName("University of Michigan--Ann Arbor");
          university.setRegion("South");
          university.setRanking("7");
          university = Univlistreg1.addUniv();
          university.setAccredation("american counsil of Education ACE");
          university.setUnivName("Wake Forest University");
          university.setRegion("South");
          university.setRanking("8");
         return Univlistreg1;
    }
    public static univRegList3 initializeUnivRegList3() {
    univRegList3 univReglist3 = new univRegList3(); 
     University university = univReglist3.addUniv();
          university.setAccredation("american counsil of Education ACE");
          university.setUnivName("Brandeis University");
          university.setRegion("West");
          university.setRanking("13");
           university = univReglist3.addUniv();
          university.setAccredation("american counsil of Education ACE");
          university.setUnivName("Georgia Institute of Technology");
          university.setRegion("West");
          university.setRanking("14");
           university = univReglist3.addUniv();
          university.setAccredation("american counsil of Education ACE");
          university.setUnivName("New York University");
          university.setRegion("West");
          university.setRanking("15");
           university = univReglist3.addUniv();
          university.setAccredation("american counsil of Education ACE");
          university.setUnivName("Case Western Reserve University");
          university.setRegion("West");
          university.setRanking("16");
    return univReglist3;
    }
     public static UnivRegList2 initializeUnivRegList2() {
      UnivRegList2 UnivReglist2 = new UnivRegList2();  
      University university = UnivReglist2.addUniv();
       university.setAccredation("american counsil of Education ACE");
          university.setUnivName("University of North Carolina--Chapel Hill");
          university.setRegion("MidWest");
          university.setRanking("9");
          university = UnivReglist2.addUniv();
          university.setAccredation("american counsil of Education ACE");
          university.setUnivName("Boston College");
          university.setRegion("MidWest");
          university.setRanking("10");
           university = UnivReglist2.addUniv();
          university.setAccredation("american counsil of Education ACE");
          university.setUnivName("College of William & Mary");
          university.setRegion("MidWest");
          university.setRanking("11");
           university = UnivReglist2.addUniv();
          university.setAccredation("american counsil of Education ACE");
          university.setUnivName("University of Rochester");
          university.setRegion("MidWest");
          university.setRanking("12");
   return UnivReglist2;    
     }
    public static univList initializeunivList() {
        univList univlist = new univList();
          University university = univlist.addUniv();
          university.setAccredation("National Association of State Universities and Land Grant Colleges");
          university.setUnivName("Emory University");
          university.setRegion("North");
          university.setRanking("1");
         university = univlist.addUniv();
          university.setAccredation("National Association of State Universities and Land Grant Colleges");
          university.setUnivName("Georgetown University");
          university.setRegion("North");
          university.setRanking("2");
   university = univlist.addUniv();
          university.setAccredation("Council for Higher Education Accreditation (CHEA)");
          university.setUnivName("University of California--Berkeley");
          university.setRegion("North");
          university.setRanking("3");
          university = univlist.addUniv();
          university.setAccredation("Council for Higher Education Accreditation (CHEA)");
          university.setUnivName("Carnegie Mellon University");
          university.setRegion("North");
          university.setRanking("4");
     
  return univlist;
    }
    public static StudentList initializeStudentList() {

        StudentList studentList = new StudentList();
        Student student = studentList.addStudent();
        student.setFirstName("nayan");
        student.setLastName("prudhvi");
        student.setId(1);
student.getGpaStudents().calculateGPA1();
        
      student = studentList.addStudent();
       student.setFirstName("divija");
        student.setLastName("neu");
        student.setId(2);
        student.getGpaStudents().calculateGPA2();
        student = studentList.addStudent();
          student.setFirstName("prithvi");
        student.setLastName("m");
        student.setId(3);
        student.getGpaStudents().calculateGPA3();
         student = studentList.addStudent();
           student.setFirstName("rahul");
        student.setLastName("d");
        student.setId(4);
        student.getGpaStudents().calculateGPA4();
        student = studentList.addStudent();
          student.setFirstName("sunny");
        student.setLastName("j");
        student.setId(5);
    student.getGpaStudents().calculateGPA5();
       
        
        return studentList;
        
       
    }
    
    
}
