/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nanda
 */
public class univRegList3 {
    
     List<University> univRegList3;
    

    public univRegList3() {
        univRegList3 = new ArrayList<>();
      
    }

    public List<University> getUnivRegList3() {
        return univRegList3;
    }

    public void setUnivRegList3(List<University> univRegList3) {
        this.univRegList3 = univRegList3;
    }
    
     public University addUniv() {
        University university = new University();
        univRegList3.add(university);
        return university;
    
    }
    
}
