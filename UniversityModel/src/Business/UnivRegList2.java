/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nanda
 */
public class UnivRegList2 {
     List<University> UnivRegList2;
    

    public UnivRegList2() {
        UnivRegList2 = new ArrayList<>();
      
    }

    public List<University> getUnivRegList2() {
        return UnivRegList2;
    }

    public void setUnivRegList2(List<University> UnivRegList2) {
        this.UnivRegList2 = UnivRegList2;
    }
    
    public University addUniv() {
        University university = new University();
        UnivRegList2.add(university);
        return university;
    
    }
    
}
