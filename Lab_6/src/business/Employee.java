/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author nanda
 */
public class Employee extends Person {

    private static int count = 1000;
    private int employeeID;
    private String organizationName;

    public Employee(String firstName, String lastName, String organizationName) {
        super(firstName, lastName);
        this.employeeID = count++;
        this.organizationName = organizationName;
    }

    public int getEmployeeID() {
        return employeeID;
    }

    public String getOrganizationName() {
        return organizationName;
    }
    
    @Override
    public String toString(){
        return this.getFirstName();
    }

}
