/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author nanda
 */
public class EmployeeDirectory {

    private ArrayList<Employee> employeeDirectory;

    public EmployeeDirectory() {
        this.employeeDirectory = new ArrayList<Employee>();
    }

    public ArrayList<Employee> getEmployeeDirectory() {
        return employeeDirectory;
    }

    public Employee addEmployee(String firstName, String lastName, String organizationName) {
        Employee employee = new Employee(firstName, lastName, organizationName);
        employeeDirectory.add(employee);
        return employee;
    }

    public void deleteEmployee(Employee employee) {
        employeeDirectory.remove(employee);
    }
}
