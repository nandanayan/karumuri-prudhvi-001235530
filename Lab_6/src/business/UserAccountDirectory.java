/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import java.util.ArrayList;

/**
 *
 * @author nanda
 */
public class UserAccountDirectory {

    private ArrayList<UserAccount> userAccountDirectory;

    public UserAccountDirectory() {
        this.userAccountDirectory = new ArrayList<UserAccount>();
    }

    public ArrayList<UserAccount> getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void addUserAccount(String userName, String password, String role, boolean isActive, Person person) {
        UserAccount userAccount = new UserAccount(userName, password, role, isActive, person);
        userAccountDirectory.add(userAccount);
    }

    public void deleteUserAccount(UserAccount userAccount) {
        userAccountDirectory.remove(userAccount);
    }
}
