/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author nanda
 */
public class UserAccount {

    public static String ADMIN_ROLE = "Admin";
    public static String EMPLOYEE_ROLE = "Employee";
    private static int count = 1000;
    private int userID;
    private String userName;
    private String password;
    private String role;
    private boolean isActive;
    private Person person;

    public UserAccount(String userName, String password, String role, boolean isActive, Person person) {
        this.userName = userName;
        this.password = password;
        this.role = role;
        this.isActive = isActive;
        this.person = person;
        this.userID = count++;
    }

    public int getUserID() {
        return userID;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public Person getPerson() {
        return person;
    }
    
    @Override
    public String toString(){
        return person.getFirstName() + " " + person.getLastName();
    }

}
