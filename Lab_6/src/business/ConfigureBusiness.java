/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

/**
 *
 * @author nanda
 */
public class ConfigureBusiness {
    public static Business initializeBusiness(){
        Business business = Business.getInstance();
        
        Employee employee = business.getEmployeeDirectory().addEmployee("Admin", " ", "NEU");
        
        business.getUserAccountDirectory().addUserAccount("admin", "admin", UserAccount.ADMIN_ROLE, true, employee);
        
        return business;
    }
}
