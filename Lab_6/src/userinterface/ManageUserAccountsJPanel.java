/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface;

import business.Business;
import business.UserAccount;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author nanda
 */
public class ManageUserAccountsJPanel extends javax.swing.JPanel {
    private JPanel userProcessContainer;
    private Business business;

    /**
     * Creates new form ManageUserAccountsJPanel
     */
    public ManageUserAccountsJPanel(JPanel userProcessContainer, Business business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;
        populateTable();
    }
    
    public void populateTable(){
        DefaultTableModel dtm = (DefaultTableModel) userAccountsJTable.getModel();
        dtm.setRowCount(0);
        
        Object[] row = new Object[3];
        for (UserAccount userAccount : business.getUserAccountDirectory().getUserAccountDirectory()) {
            row[0] = userAccount;
            row[1] = userAccount.getUserName();
            row[2] = userAccount.getRole();
            dtm.addRow(row);
        } 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        backJButton = new javax.swing.JButton();
        manageUserAccountsJLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        userAccountsJTable = new javax.swing.JTable();
        deleteUserAccountJButton = new javax.swing.JButton();
        addUserAccountJButton = new javax.swing.JButton();

        backJButton.setText("<<Head Back To Previous Page");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        manageUserAccountsJLabel.setFont(new java.awt.Font("Tahoma", 1, 25)); // NOI18N
        manageUserAccountsJLabel.setText("MANAGE USER ACCOUNTS");

        userAccountsJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PERSON", "USER NAME", "ROLE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(userAccountsJTable);

        deleteUserAccountJButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        deleteUserAccountJButton.setText("Remove a User Account");
        deleteUserAccountJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteUserAccountJButtonActionPerformed(evt);
            }
        });

        addUserAccountJButton.setFont(new java.awt.Font("Tahoma", 0, 20)); // NOI18N
        addUserAccountJButton.setText("Add a User Account");
        addUserAccountJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addUserAccountJButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 837, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(backJButton)
                        .addGap(130, 130, 130)
                        .addComponent(manageUserAccountsJLabel))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(addUserAccountJButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(deleteUserAccountJButton, javax.swing.GroupLayout.Alignment.LEADING)))
                .addContainerGap(114, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(backJButton)
                    .addComponent(manageUserAccountsJLabel))
                .addGap(45, 45, 45)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(addUserAccountJButton)
                .addGap(18, 18, 18)
                .addComponent(deleteUserAccountJButton)
                .addContainerGap(249, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addUserAccountJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addUserAccountJButtonActionPerformed
        // TODO add your handling code here:
        AddUserAccountJpanel panel = new AddUserAccountJpanel(userProcessContainer, business, this);
        userProcessContainer.add("AddUserAccountJpanel", panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_addUserAccountJButtonActionPerformed

    private void deleteUserAccountJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteUserAccountJButtonActionPerformed
        // TODO add your handling code here:
        int selectedRow = userAccountsJTable.getSelectedRow();
        if (selectedRow >= 0) {
            int dialog = JOptionPane.showConfirmDialog(this, "ARE YOU SURE YOU WANT TO DELETE THIS UserAccount?", "CONFIRMATION REQUIRED", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            
            if (dialog == JOptionPane.YES_OPTION) {
              UserAccount userAccount = (UserAccount) userAccountsJTable.getValueAt(selectedRow, 1);
              business.getUserAccountDirectory().deleteUserAccount(userAccount);
              populateTable();
            }
            
        } else {
            JOptionPane.showMessageDialog(this, "Please Select a Row From The Table", "NO INPUT RECOGNIZED", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_deleteUserAccountJButtonActionPerformed

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addUserAccountJButton;
    private javax.swing.JButton backJButton;
    private javax.swing.JButton deleteUserAccountJButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel manageUserAccountsJLabel;
    private javax.swing.JTable userAccountsJTable;
    // End of variables declaration//GEN-END:variables
}
