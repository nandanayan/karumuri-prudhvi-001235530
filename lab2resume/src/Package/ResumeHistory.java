/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Package;
import java.util.ArrayList;
/**
 *
 * @author nanda
 */
public class ResumeHistory {
    private ArrayList<Resume> resumeHistory;
    
    public ResumeHistory()
    {
        resumeHistory=new ArrayList<Resume>();
        
    }

    public ArrayList<Resume> getResumeHistory() {
        return resumeHistory;
    }

    public void setResumeHistory(ArrayList<Resume> resumeHistory) {
        this.resumeHistory = resumeHistory;
    }
    
    public Resume addVitals()
    {
        Resume vs = new Resume();
        resumeHistory.add(vs);
        return vs;
    }
   public void deleteResume(Resume v)
           
   {
       resumeHistory.remove(v);
       
   }
    
}
