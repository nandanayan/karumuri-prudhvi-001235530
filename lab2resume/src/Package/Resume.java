/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Package;

/**
 *
 * @author nanda
 */
public class Resume {
    private String firstName;
    private String lastName;
    private String image;
    private String adressLine1;
    private String adressLine2;
    private String city;
    private String state;
    private String country;
    private String zipCode;
    private String mobileNumber;
    private String emailAdress;
    private String citizenship;
    private String affiliation;
    private String gender;
    private String careerObjective;
    private String Schoolnamegrad;
    private String Schoolnameug;
    private String Schoolnamehs;
    private String startDategrad;
    private String endDategrad;
    private String endDateug;
    private String endDatehs;
    private String startDateug;
    private String startDatehs;
    private String majorug;
    private String majorgrad;
    private String majorhs;
    private String AcademicAchvmntgrad;
    private String AcademicAchvmntug;
    private String AcademicAchvmnths;
    private String gpagrad;
    private String gpaug;
    private String gpahs;
    private String recentOrganisation;
    private String jobProfile;
    private String hadoop;
    private String expInMonths;
    private String Skillset;
    private String certifications;
    private String awardsAndHonors;
    private String areasOfInterest;
    private String skl;
    private String pastEXperience;
    private String jobinfo;

    public String getJobinfo() {
        return jobinfo;
    }

    public void setJobinfo(String jobinfo) {
        this.jobinfo = jobinfo;
    }

    public String getPastEXperience() {
        return pastEXperience;
    }

    public void setPastEXperience(String pastEXperience) {
        this.pastEXperience = pastEXperience;
    }

   
    

    public String getSkl() {
        return skl;
    }

    public void setSkl(String skl) {
        this.skl = skl;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAdressLine1() {
        return adressLine1;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setAdressLine1(String adressLine1) {
        this.adressLine1 = adressLine1;
    }

    public String getAdressLine2() {
        return adressLine2;
    }

    public void setAdressLine2(String adressLine2) {
        this.adressLine2 = adressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmailAdress() {
        return emailAdress;
    }

    public void setEmailAdress(String emailAdress) {
        this.emailAdress = emailAdress;
    }

    public String getAffiliation() {
        return affiliation;
    }

    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    public String getCareerObjective() {
        return careerObjective;
    }

    public void setCareerObjective(String careerObjective) {
        this.careerObjective = careerObjective;
    }

    public String getSchoolnamegrad() {
        return Schoolnamegrad;
    }

    public void setSchoolnamegrad(String Schoolnamegrad) {
        this.Schoolnamegrad = Schoolnamegrad;
    }

    public String getSchoolnameug() {
        return Schoolnameug;
    }

    public void setSchoolnameug(String Schoolnameug) {
        this.Schoolnameug = Schoolnameug;
    }

    public String getSchoolnamehs() {
        return Schoolnamehs;
    }

    public void setSchoolnamehs(String Schoolnamehs) {
        this.Schoolnamehs = Schoolnamehs;
    }

    public String getStartDategrad() {
        return startDategrad;
    }

    public void setStartDategrad(String startDategrad) {
        this.startDategrad = startDategrad;
    }

    public String getEndDategrad() {
        return endDategrad;
    }

    public void setEndDategrad(String endDategrad) {
        this.endDategrad = endDategrad;
    }

    public String getEndDateug() {
        return endDateug;
    }

    public void setEndDateug(String endDateug) {
        this.endDateug = endDateug;
    }

    public String getEndDatehs() {
        return endDatehs;
    }

    public void setEndDatehs(String endDatehs) {
        this.endDatehs = endDatehs;
    }

    public String getStartDateug() {
        return startDateug;
    }

    public void setStartDateug(String startDateug) {
        this.startDateug = startDateug;
    }

    public String getStartDatehs() {
        return startDatehs;
    }

    public void setStartDatehs(String startDatehs) {
        this.startDatehs = startDatehs;
    }

    public String getMajorug() {
        return majorug;
    }

    public void setMajorug(String majorug) {
        this.majorug = majorug;
    }

    public String getMajorgrad() {
        return majorgrad;
    }

    public void setMajorgrad(String majorgrad) {
        this.majorgrad = majorgrad;
    }

    public String getMajorhs() {
        return majorhs;
    }

    public void setMajorhs(String majorhs) {
        this.majorhs = majorhs;
    }

    public String getAcademicAchvmntgrad() {
        return AcademicAchvmntgrad;
    }

    public void setAcademicAchvmntgrad(String AcademicAchvmntgrad) {
        this.AcademicAchvmntgrad = AcademicAchvmntgrad;
    }

    public String getAcademicAchvmntug() {
        return AcademicAchvmntug;
    }

    public void setAcademicAchvmntug(String AcademicAchvmntug) {
        this.AcademicAchvmntug = AcademicAchvmntug;
    }

    public String getAcademicAchvmnths() {
        return AcademicAchvmnths;
    }

    public void setAcademicAchvmnths(String AcademicAchvmnths) {
        this.AcademicAchvmnths = AcademicAchvmnths;
    }

    public String getGpagrad() {
        return gpagrad;
    }

    public void setGpagrad(String gpagrad) {
        this.gpagrad = gpagrad;
    }

    public String getGpaug() {
        return gpaug;
    }

    public void setGpaug(String gpaug) {
        this.gpaug = gpaug;
    }

    public String getGpahs() {
        return gpahs;
    }

    public void setGpahs(String gpahs) {
        this.gpahs = gpahs;
    }

    public String getRecentOrganisation() {
        return recentOrganisation;
    }

    public void setRecentOrganisation(String recentOrganisation) {
        this.recentOrganisation = recentOrganisation;
    }

    public String getJobProfile() {
        return jobProfile;
    }

    public void setJobProfile(String jobProfile) {
        this.jobProfile = jobProfile;
    }

    public String getExpInMonths() {
        return expInMonths;
    }

    public void setExpInMonths(String expInMonths) {
        this.expInMonths = expInMonths;
    }

    public String getSkillset() {
        return Skillset;
    }

    public void setSkillset(String Skillset) {
        this.Skillset = Skillset;
    }

    public String getCertifications() {
        return certifications;
    }

    public void setCertifications(String certifications) {
        this.certifications = certifications;
    }

    public String getAwardsAndHonors() {
        return awardsAndHonors;
    }

    public void setAwardsAndHonors(String awardsAndHonors) {
        this.awardsAndHonors = awardsAndHonors;
    }

    public String getAreasOfInterest() {
        return areasOfInterest;
    }

    public void setAreasOfInterest(String areasOfInterest) {
        this.areasOfInterest = areasOfInterest;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getHadoop() {
        return hadoop;
    }

    public void setHadoop(String hadoop) {
        this.hadoop = hadoop;
    }
@Override
    public String toString(){
        return this.emailAdress;
    }
        
}
